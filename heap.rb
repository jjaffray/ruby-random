require "minitest/autorun"

class Heap
  attr_reader :values

  def initialize
    @values = []
  end

  def pop
    value = values.first
    values[0] = values.pop unless values.empty?
    heap_down(0)
    value
  end

  def <<(val)
    values << val
    heap_up(values.count - 1)
  end

  private

  def heap_up(index)
    return if root? index
    if values[index] > values[parent(index)]
      swap(index, parent(index))
      heap_up(parent(index))
    end
  end

  def heap_down(index)
    return unless has_child?(index)
    if values[right(index)].nil? || values[left(index)] > values[right(index)]
      heap_down_to(index, left(index))
    else
      heap_down_to(index, right(index))
    end
  end

  def heap_down_to(index, to)
    if values[to] > values[index]
      swap(index, to)
      heap_down(to)
    end
  end

  def swap(a, b)
    values[a], values[b] = values[b], values[a]
  end

  def root?(index)
    index == 0
  end

  def parent(index)
    ((index - 1) / 2).floor
  end

  def left(index)
    index * 2 + 1
  end

  def right(index)
    index * 2 + 2
  end
  
  def has_child?(index)
    left(index) < values.count
  end
end

describe Heap do
  it "returns nil if popped when empty" do
    Heap.new.pop.must_equal nil
  end

  it "pops the thing put in if there is only one" do
    h = Heap.new
    h << 5
    h.pop.must_equal 5
  end

  it "pops the largest of multiple things" do
    h = Heap.new
    h << 2
    h << 3
    h << 1
    h.pop.must_equal 3
    h.pop.must_equal 2
    h.pop.must_equal 1
  end

  it "re-heaps when things are removed" do
    h = Heap.new
    h << 2
    h << 1
    h << 3
    h << 4
    h << 5
    h.pop.must_equal 5
    h.pop.must_equal 4
    h.pop.must_equal 3
    h.pop.must_equal 2
    h.pop.must_equal 1
  end
end
