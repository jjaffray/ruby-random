require "minitest/autorun"

class Node < Struct.new(:data, :succ)
end

class LinkedList
  include Enumerable

  attr_accessor :head
  def initialize
    @head = nil
  end

  def each
    node = @head
    until node.nil?
      yield node.data
      node = node.succ
    end
  end

  def <<(value)
    new_node = Node.new(value, nil)

    if @head.nil?
      @head = new_node
    else
      tail = @head
      tail = @head.succ until tail.succ.nil?
      tail.succ = new_node
    end
  end

  def [](position)
    node = @head
    position.times { node = node.succ }
    node.data
  end
end

describe LinkedList do
  it "lets you add a thing" do
    n = LinkedList.new
    n << 5
    n[0].must_equal 5
  end

  it "lets you add multiple things" do
    n = LinkedList.new
    n << 1
    n << 2
    n[0].must_equal 1
    n[1].must_equal 2
  end

  it "implements enumerable" do
    n = LinkedList.new
    n << 1
    n << 2
    n.entries.must_equal [1,2]
    n.map { |x| x * x }.must_equal [1,4]
  end
end
