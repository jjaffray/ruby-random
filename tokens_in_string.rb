# Given an array, find the shortest subarray which contains every element.
#
# the idea is to begin at the left side and extend our substring until it contains every token.
# after that, begin compressing from the left side until we can't anymore, then begin extending
# from the right side again, and so on. 
#
# take the minimum substring that you encounter after every expansion and contraction to get a list
# of every local minimum, then simply take the smallest of those.
#
# as a proof that this will find the minimum value, just note that it will find every local minimum,
# since it will extend until it can't extend any more, then compress until it can't compress any more,
# and repeat. by taking the best of all these local minima, we must find the global minimum.

def valid_substrings(s)
  Enumerator.new do |results|
    ending = 0
    current_counts = {}
    s.uniq.each { |token| current_counts[token] = 0 }

    (0...s.length).each do |beginning|
      until current_counts.values.none?(&:zero?) || ending >= s.count
        current_counts[s[ending]] += 1
        ending += 1
      end
      can_contract_more = current_counts[s[beginning]] > 1 # minor optimization
      results << s[beginning...ending] if current_counts.values.none?(&:zero?) && !can_contract_more
      current_counts[s[beginning]] -= 1
    end
  end
end

def shortest_substring(s)
  valid_substrings(s).min_by(&:length)
end

p shortest_substring([:a, :a, :b, :a, :c, :c, :a, :a, :a, :a, :d, :b, :c, :d])
  # => [:a, :d, :b, :c]
