require "minitest/autorun"

###
# 
# Turn on $VISUALIZE to see all the comparisons.

$VISUALIZE = true

class BoyerMooreMatcher
  attr_reader :last_occurrence
  attr_reader :bad_suffix

  def initialize(needle)
    @needle = needle.chars
    @last_occurrence = build_last_occurrence
    @bad_suffix = build_bad_suffix
  end

  def match(input)
    haystack = input.chars
    index = 0
    results = []

    puts "\n" if $VISUALIZE
    puts haystack.join if $VISUALIZE

    until index + @needle.count > haystack.count
      delta = @needle.count - 1
      puts " " * index + @needle.join if $VISUALIZE
      until delta == -1 || @needle[delta] != haystack[index + delta]
        delta -= 1
      end

      if delta == -1
        results << index 
        delta = 0
      end

      lc = last_occurrence[haystack[index + delta]]
      bs = bad_suffix[delta]
      index += delta - [lc, bs].min
    end
    results
  end

  private

  def bad_suffix_for(chars)
    index = @needle.count - chars.count
    index -= 1 until matches_at(index, chars)
    index
  end

  def build_last_occurrence
    @needle.uniq.each_with_object(Hash.new(-1)) do |char, hsh|
      hsh[char] = last_occurrence_of(char)
    end
  end

  def last_occurrence_of(char)
    @needle.length - @needle.reverse.find_index(char) - 1
  end

  def build_bad_suffix
    (1..@needle.count).to_a.reverse.map { |i| bad_suffix_for(@needle.last i) }
  end

  def matches_at(index, chars)
    substr = @needle[index + 1...index + chars.count]
    good_match = chars.drop(1).each_with_index.all? { |char, i| match_at(char, index + i + 1) }
    nonmatch_at(chars.first, index) && good_match
  end

  def nonmatch_at(char, index)
    index < 0 || char != @needle[index]
  end

  def match_at(char, index)
    index < 0 || char == @needle[index]
  end
end

describe BoyerMooreMatcher do
  let(:matcher) { BoyerMooreMatcher.new "abacaba" }
  describe "last occurrence dictionary" do
    let(:lo) { matcher.last_occurrence }
    it "contains the last occurrence of characters" do
      # a b a c a b a
      #       ^   ^ ^
      lo["a"].must_equal 6
      lo["b"].must_equal 5
      lo["c"].must_equal 3
    end

    it "is -1 for characters not occurring" do
      lo["x"].must_equal -1
    end
  end

  describe "bad suffix array" do
    it "has the index of the last occurence of NOT the current character, but the same suffix" do
      matcher.bad_suffix.must_equal [-4, -3, -2, -1, -2, 3, 5]
    end
  end

  describe "matching" do
    it "returns an enumeration of all the matching indices" do
      matcher.match("abacaba").must_equal [0]
      matcher.match("aabacaba").must_equal [1]
      matcher.match("abacabacaba").must_equal [0, 4]
      matcher.match("hello my name is abacaba").must_equal [17]
    end
  end
end
