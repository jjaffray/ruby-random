require "minitest/autorun"

class RomanNumerals
  class << self
    def for(n)
      result = ""
      [
        ["X",  10],
        ["V",  5 ],
        ["IV", 4 ],
        ["I",  1 ],
      ].each do |glyph, value|
        while n >= value
          result += glyph
          n -= value
        end
      end

      result
    end
  end
end

describe RomanNumerals do

  def compute(n)
    RomanNumerals.for(n)
  end
 
  it "gives blank for 0" do
    compute(0).must_equal ""
  end

  it "gives I for 1" do
    compute(1).must_equal "I"
  end

  it "gives II for 2" do
    compute(2).must_equal "II"
  end

  it "gives IV for 4" do
    compute(4).must_equal "IV"
  end

  it "gives V for 5" do
    compute(5).must_equal "V"
  end

  it "gives VI for 6" do
    compute(6).must_equal "VI"
  end

  it "gives X for 10" do
    compute(10).must_equal "X"
  end

  it "gives XX for 20" do
    compute(20).must_equal "XX"
  end
end
