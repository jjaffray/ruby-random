# parses very simple arithmetic expressions like
# 1 + 2
# 1 + 4 * 7
# 4 / 2 * 4
#
# while properly following order of operations

class Arithmetic
  class << self
    def eval(s)
      run(treeify(parse(s)))
    end

    def run(node)
      root = node.first
      left = node[1]
      right = node[2]

      kind, lexeme = root
      case kind
      when :-
        run(left) - run(right)
      when :+
        run(left) + run(right)
      when :*
        run(left) * run(right)
      when :/
        run(left) / run(right)
      when :number
        lexeme.to_i
      end
    end

    def parse(s)
      s = s.gsub(/\s/, "")
      result = []
      until s.empty?
        i = 0
        until complete_token?(s, i)
          i += 1
          raise "could not tokenize \"#{s}\"" if i >= s.length && !kind(s[0...i])
        end
        result << [kind(s[0...i]), s[0...i]]
        s = s[i...s.length]
      end
      result
    end

    def complete_token?(input, upto)
      kind(input[0...upto]) && (!kind(input[0...upto+1]) || upto >= input.length)
    end

    def treeify(parsed)
      return parsed if parsed.count == 1
      [[:-, :+], [:*, :/], [:number]].each do |splitter|
        index = parsed.reverse.find_index { |kind, _| splitter.include? kind }
        if index
          index = parsed.count - 1 - index
          left = parsed[0...index]
          right = parsed[index+1...parsed.count]
          root = parsed[index]
          return [root, treeify(left), treeify(right)]
        end
      end
    end

    def kind(str)
      case str
      when /^\d+$/
        :number
      when "-"
        :-
      when "+"
        :+
      when "*"
        :*
      when "/"
        :/
      end
    end
  end
end

p Arithmetic.eval("1 + 2 * 3 / 3")
